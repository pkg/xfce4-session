# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2019-2023
# José Vieira <jvieira33@sapo.pt>, 2016,2019-2020
# Nuno “Nishita” Donato <nunodonato@gmail.com>, 2004
# Nuno Miguel <nunomgue@gmail.com>, 2016-2020
# Rui <xymarior@yandex.com>, 2019
msgid ""
msgstr ""
"Project-Id-Version: Xfce4-session\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-28 15:00+0100\n"
"PO-Revision-Date: 2013-07-02 20:44+0000\n"
"Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>, 2019-2023\n"
"Language-Team: Portuguese (http://www.transifex.com/xfce/xfce4-session/language/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: ../xfce.desktop.in.h:1
msgid "Xfce Session"
msgstr "Sessão Xfce"

#: ../xfce.desktop.in.h:2
msgid "Use this session to run Xfce as your desktop environment"
msgstr "Utilizar esta sessão para executar o Xfce como ambiente de trabalho"

#: ../libxfsm/xfsm-util.c:331
msgid "Session"
msgstr "Sessão"

#: ../libxfsm/xfsm-util.c:342
msgid "Last accessed"
msgstr "Último acesso"

#: ../scripts/xscreensaver.desktop.in.h:1
msgid "Screensaver"
msgstr "Proteção de ecrã"

#: ../scripts/xscreensaver.desktop.in.h:2
msgid "Launch screensaver and locker program"
msgstr "Lançar proteção de ecrã e programa de bloqueio"

#: ../settings/main.c:99
msgid "Settings manager socket"
msgstr "Socket do gestor de definições"

#: ../settings/main.c:99
msgid "SOCKET ID"
msgstr "ID do SOCKET"

#: ../settings/main.c:100
msgid "Version information"
msgstr "Informações da versão"

#: ../settings/main.c:111 ../xfce4-session/main.c:324
#, c-format
msgid "Type '%s --help' for usage."
msgstr "Digite '%s --help' para informações de utilização."

#: ../settings/main.c:123 ../xfce4-session/main.c:334
#: ../xfce4-session-logout/main.c:146
msgid "The Xfce development team. All rights reserved."
msgstr "A equipa de desenvolvimento do Xfce. Todos os direitos reservados."

#: ../settings/main.c:124 ../xfce4-session/main.c:335
#: ../xfce4-session-logout/main.c:149
#, c-format
msgid "Please report bugs to <%s>."
msgstr "Por favor, comunique os erros em <%s>."

#: ../settings/main.c:133 ../xfce4-session/main.c:343
msgid "Unable to contact settings server"
msgstr "Incapaz de contactar o servidor de definições"

#: ../settings/main.c:153
msgid "Unable to create user interface from embedded definition data"
msgstr "Incapaz de criar a interface de utilizador para os dados das definições"

#: ../settings/main.c:167
msgid "App_lication Autostart"
msgstr "Aplicações a_utomáticas"

#: ../settings/main.c:173
msgid "Currently active session:"
msgstr "Sessão ativa atualmente:"

#: ../settings/session-editor.c:63
msgid "If running"
msgstr "Se em execução"

#: ../settings/session-editor.c:64
msgid "Always"
msgstr "Sempre"

#: ../settings/session-editor.c:65
msgid "Immediately"
msgstr "Imediatamente"

#: ../settings/session-editor.c:66
msgid "Never"
msgstr "Nunca"

#: ../settings/session-editor.c:138
msgid "Session Save Error"
msgstr "Erro ao guardar sessão"

#: ../settings/session-editor.c:139
msgid "Unable to save the session"
msgstr "Incapaz de guardar a sessão"

#: ../settings/session-editor.c:141 ../settings/session-editor.c:314
#: ../xfce4-session/xfsm-manager.c:1282
#: ../settings/xfce4-session-settings.ui.h:6
msgid "_Close"
msgstr "_Fechar"

#: ../settings/session-editor.c:199
msgid "Clear sessions"
msgstr "Apagar sessões"

#: ../settings/session-editor.c:200
msgid "Are you sure you want to empty the session cache?"
msgstr "Tem a certeza de que quer limpar a cache de sessões?"

#: ../settings/session-editor.c:201
msgid ""
"The saved states of your applications will not be restored during your next "
"login."
msgstr "O estado das aplicações não será restaurado na próxima sessão."

#: ../settings/session-editor.c:202 ../settings/session-editor.c:289
#: ../settings/xfae-dialog.c:77 ../xfce4-session/xfsm-manager.c:683
#: ../xfce4-session/xfsm-logout-dialog.c:221
msgid "_Cancel"
msgstr "_Cancelar"

#: ../settings/session-editor.c:203
msgid "_Proceed"
msgstr "_Prosseguir"

#: ../settings/session-editor.c:241
#, c-format
msgid "You might need to delete some files manually in \"%s\"."
msgstr "Pode ter que eliminar alguns ficheiros manualmente em \"%s\"."

#: ../settings/session-editor.c:244
msgid "All Xfce cache files could not be cleared"
msgstr "Os ficheiros da cache Xfce não foram eliminados"

#: ../settings/session-editor.c:283
#, c-format
msgid "Are you sure you want to terminate \"%s\"?"
msgstr "Tem a certeza que quer terminar \"%s\"?"

#: ../settings/session-editor.c:286 ../settings/session-editor.c:311
msgid "Terminate Program"
msgstr "Terminar programa"

#: ../settings/session-editor.c:288
msgid ""
"The application will lose any unsaved state and will not be restarted in "
"your next session."
msgstr "A aplicação irá perder qualquer estado não guardado e não será reiniciada na sessão seguinte."

#: ../settings/session-editor.c:290 ../settings/xfce4-session-settings.ui.h:21
msgid "_Quit Program"
msgstr "_Sair do programa"

#: ../settings/session-editor.c:312
msgid "Unable to terminate program."
msgstr "Incapaz de terminar programa."

#: ../settings/session-editor.c:537
msgid "(Unknown program)"
msgstr "(Programa desconhecido)"

#: ../settings/session-editor.c:784
msgid "Priority"
msgstr "Prioridade"

#: ../settings/session-editor.c:794
msgid "PID"
msgstr "PID"

#: ../settings/session-editor.c:801 ../settings/xfae-window.c:190
msgid "Program"
msgstr "Programa"

#: ../settings/session-editor.c:827
msgid "Restart Style"
msgstr "Reiniciar estilo"

#: ../settings/xfae-dialog.c:78 ../xfce4-session/xfsm-manager.c:685
msgid "_OK"
msgstr "_OK"

#: ../settings/xfae-dialog.c:82 ../settings/xfae-window.c:244
msgid "Add application"
msgstr "Adicionar aplicação"

#: ../settings/xfae-dialog.c:96
msgid "Name:"
msgstr "Nome:"

#: ../settings/xfae-dialog.c:111
msgid "Description:"
msgstr "Descrição:"

#: ../settings/xfae-dialog.c:125 ../settings/xfae-model.c:687
msgid "Command:"
msgstr "Comando:"

#: ../settings/xfae-dialog.c:138
msgid "Trigger:"
msgstr "Acionar:"

#: ../settings/xfae-dialog.c:209
msgid "Select a command"
msgstr "Selecione um comando"

#: ../settings/xfae-dialog.c:212
msgid "Cancel"
msgstr "Cancelar"

#: ../settings/xfae-dialog.c:213
msgid "OK"
msgstr "OK"

#: ../settings/xfae-dialog.c:260 ../settings/xfae-window.c:266
msgid "Edit application"
msgstr "Editar aplicação"

#: ../settings/xfae-model.c:114
msgid "on login"
msgstr "ao entrar"

#: ../settings/xfae-model.c:115
msgid "on logout"
msgstr "ao sair"

#: ../settings/xfae-model.c:116
msgid "on shutdown"
msgstr "ao encerrar"

#: ../settings/xfae-model.c:117
msgid "on restart"
msgstr "ao reiniciar"

#: ../settings/xfae-model.c:118
msgid "on suspend"
msgstr "ao suspender"

#: ../settings/xfae-model.c:119
msgid "on hibernate"
msgstr "ao hibernar"

#: ../settings/xfae-model.c:120
msgid "on hybrid sleep"
msgstr "na suspensão híbrida"

#: ../settings/xfae-model.c:121
msgid "on switch user"
msgstr "ao mudar de utilizador"

#: ../settings/xfae-model.c:463 ../settings/xfae-model.c:1182
#: ../settings/xfae-model.c:1240
#, c-format
msgid "Failed to open %s for writing"
msgstr "Falha ao abrir %s para escrita"

#: ../settings/xfae-model.c:820
#, c-format
msgid "Failed to unlink %s: %s"
msgstr "Falha ao remover a ligação %s: %s"

#: ../settings/xfae-model.c:982
#, c-format
msgid "Failed to create file %s"
msgstr "Falha ao criar o ficheiro %s"

#: ../settings/xfae-model.c:1006
#, c-format
msgid "Failed to write file %s"
msgstr "Falha ao escrever o ficheiro %s"

#: ../settings/xfae-model.c:1066
#, c-format
msgid "Failed to open %s for reading"
msgstr "Falha ao abrir %s para leitura"

#: ../settings/xfae-window.c:100
msgid "Failed to set run hook"
msgstr "Falha ao estabelecer gancho executável"

#: ../settings/xfae-window.c:215
msgid "Trigger"
msgstr "Acionador"

#: ../settings/xfae-window.c:241 ../settings/xfae-window.c:330
msgid "Add"
msgstr "Adicionar"

#: ../settings/xfae-window.c:250 ../settings/xfae-window.c:336
msgid "Remove"
msgstr "Remover"

#: ../settings/xfae-window.c:253
msgid "Remove application"
msgstr "Remover aplicação"

#: ../settings/xfae-window.c:263
msgid "Edit"
msgstr "Editar"

#: ../settings/xfae-window.c:390
#, c-format
msgid "Failed adding \"%s\""
msgstr "Falha ao adicionar \"%s\""

#: ../settings/xfae-window.c:421 ../settings/xfae-window.c:435
msgid "Failed to remove item"
msgstr "Falha ao remover item"

#: ../settings/xfae-window.c:464
msgid "Failed to edit item"
msgstr "Falha ao editar item"

#: ../settings/xfae-window.c:484
#, c-format
msgid "Failed to edit item \"%s\""
msgstr "Falha ao editar o item \"%s\""

#: ../settings/xfae-window.c:512
msgid "Failed to toggle item"
msgstr "Falha ao trocar de item"

#: ../xfce4-session/main.c:77
msgid "Disable binding to TCP ports"
msgstr "Desativar ligação às portas TCP"

#: ../xfce4-session/main.c:78 ../xfce4-session-logout/main.c:102
msgid "Print version information and exit"
msgstr "Mostra a informação da versão e sai"

#: ../xfce4-session/xfsm-chooser.c:147
msgid "Session Manager"
msgstr "Gestor de sessão"

#: ../xfce4-session/xfsm-chooser.c:168
msgid ""
"Choose the session you want to restore. You can simply double-click the "
"session name to restore it."
msgstr "Escolha a sessão que quer restaurar. Basta clicar duas vezes no nome da sessão para a restaurar."

#: ../xfce4-session/xfsm-chooser.c:184
msgid "Create a new session."
msgstr "Criar uma nova sessão."

#: ../xfce4-session/xfsm-chooser.c:191
msgid "Delete a saved session."
msgstr "Eliminar uma sessão guardada."

#. "Logout" button
#: ../xfce4-session/xfsm-chooser.c:202
#: ../xfce4-session-logout/xfce4-session-logout.desktop.in.h:1
msgid "Log Out"
msgstr "Sair"

#: ../xfce4-session/xfsm-chooser.c:204
msgid "Cancel the login attempt and return to the login screen."
msgstr "Cancelar o início de sessão e regressar ao ecrã de entrada."

#. "Start" button
#: ../xfce4-session/xfsm-chooser.c:211
msgid "Start"
msgstr "Iniciar"

#: ../xfce4-session/xfsm-chooser.c:212
msgid "Start an existing session."
msgstr "Iniciar uma sessão existente."

#: ../xfce4-session/xfsm-dns.c:78
msgid "(Unknown)"
msgstr "(Desconhecido)"

#: ../xfce4-session/xfsm-dns.c:152
#, c-format
msgid ""
"Could not look up internet address for %s.\n"
"This will prevent Xfce from operating correctly.\n"
"It may be possible to correct the problem by adding\n"
"%s to the file /etc/hosts on your system."
msgstr "Não foi possível encontrar o endereço web de %s.\nIsto irá impedir o correto funcionamento do Xfce.\nPode tentar corrigir este problema adicionando\n%s ao ficheiro /etc/hosts do seu sistema."

#: ../xfce4-session/xfsm-dns.c:159
msgid "Continue anyway"
msgstr "Continuar, ainda assim"

#: ../xfce4-session/xfsm-dns.c:160
msgid "Try again"
msgstr "Tentar novamente"

#: ../xfce4-session/xfsm-manager.c:567
#, c-format
msgid ""
"Unable to determine failsafe session name.  Possible causes: xfconfd isn't "
"running (D-Bus setup problem); environment variable $XDG_CONFIG_DIRS is set "
"incorrectly (must include \"%s\"), or xfce4-session is installed "
"incorrectly."
msgstr "Incapaz de determinar o nome de sessão failsafe. Causas possíveis: xfconfd não está em execução (problema de configuração do D-Bus); variável de ambiente $XDG_CONFIG_DIRS incorreta (deve incluir \"%s\") ou xfce4-session mal instalado."

#: ../xfce4-session/xfsm-manager.c:578
#, c-format
msgid ""
"The specified failsafe session (\"%s\") is not marked as a failsafe session."
msgstr "A sessão failsafe especificada (\"%s\") não é reconhecida como uma sessão failsafe."

#: ../xfce4-session/xfsm-manager.c:611
msgid "The list of applications in the failsafe session is empty."
msgstr "A lista de aplicações da sessão failsafe está vazia."

#: ../xfce4-session/xfsm-manager.c:697
msgid "Name for the new session"
msgstr "Nome da nova sessão"

#. FIXME: migrate this into the splash screen somehow so the
#. * window doesn't look ugly (right now no WM is running, so it
#. * won't have window decorations).
#: ../xfce4-session/xfsm-manager.c:775
msgid "Session Manager Error"
msgstr "Erro do gestor de sessões"

#: ../xfce4-session/xfsm-manager.c:777
msgid "Unable to load a failsafe session"
msgstr "Incapaz de carregar a sessão \"failsafe\""

#: ../xfce4-session/xfsm-manager.c:779
msgid "_Quit"
msgstr "_Sair"

#: ../xfce4-session/xfsm-manager.c:1272
msgid "Shutdown Failed"
msgstr "Falha ao desligar"

#: ../xfce4-session/xfsm-manager.c:1275
msgid "Failed to suspend session"
msgstr "Falha ao suspender a sessão"

#: ../xfce4-session/xfsm-manager.c:1277
msgid "Failed to hibernate session"
msgstr "Falha ao hibernar a sessão"

#: ../xfce4-session/xfsm-manager.c:1279
msgid "Failed to hybrid sleep session"
msgstr "Falhou a suspensão híbrida da sessão"

#: ../xfce4-session/xfsm-manager.c:1280
msgid "Failed to switch user"
msgstr "Falha ao trocar de utilizador"

#: ../xfce4-session/xfsm-manager.c:1586
#, c-format
msgid "Can only terminate clients when in the idle state"
msgstr "Só pode terminar clientes quando em estado inativo"

#: ../xfce4-session/xfsm-manager.c:2250
msgid "Session manager must be in idle state when requesting a checkpoint"
msgstr "O gestor de sessões tem de estar inativo ao solicitar um ponto de verificação"

#: ../xfce4-session/xfsm-manager.c:2320 ../xfce4-session/xfsm-manager.c:2340
msgid "Session manager must be in idle state when requesting a shutdown"
msgstr "O gestor de sessões tem de estar inativo ao solicitar o encerramento"

#: ../xfce4-session/xfsm-manager.c:2385
msgid "Session manager must be in idle state when requesting a restart"
msgstr "Gestor de sessão tem de estar em modo de pausa para reiniciar"

#: ../xfce4-session/xfsm-logout-dialog.c:193
#, c-format
msgid "Log out %s"
msgstr "Terminar sessão de %s"

#. *
#. * Logout
#. *
#: ../xfce4-session/xfsm-logout-dialog.c:238
msgid "_Log Out"
msgstr "_Sair"

#: ../xfce4-session/xfsm-logout-dialog.c:258
msgid "_Restart"
msgstr "_Reiniciar"

#: ../xfce4-session/xfsm-logout-dialog.c:278
msgid "Shut _Down"
msgstr "_Desligar"

#: ../xfce4-session/xfsm-logout-dialog.c:302
msgid "Sus_pend"
msgstr "Sus_pender"

#: ../xfce4-session/xfsm-logout-dialog.c:336
msgid "_Hibernate"
msgstr "_Hibernar"

#: ../xfce4-session/xfsm-logout-dialog.c:367
msgid "H_ybrid Sleep"
msgstr "S_uspensão híbrida"

#: ../xfce4-session/xfsm-logout-dialog.c:398
msgid "Switch _User"
msgstr "Trocar de _utilizador"

#: ../xfce4-session/xfsm-logout-dialog.c:423
msgid "_Save session for future logins"
msgstr "_Guardar sessão para futuras utilizações"

#: ../xfce4-session/xfsm-logout-dialog.c:451
msgid "An error occurred"
msgstr "Ocorreu um erro"

#: ../xfce4-session/xfsm-shutdown.c:159
msgid "Shutdown is blocked by the kiosk settings"
msgstr "Encerramento está bloqueado pelas definições \"kiosk\""

#: ../xfce4-session/xfsm-shutdown.c:216
#, c-format
msgid "Unknown shutdown method %d"
msgstr "Método de encerramento desconhecido %d"

#: ../xfce4-session-logout/main.c:70
msgid "Log out without displaying the logout dialog"
msgstr "Sair sem mostrar a caixa de diálogo de saída"

#: ../xfce4-session-logout/main.c:74
msgid "Halt without displaying the logout dialog"
msgstr "Parar sem mostrar a caixa de diálogo de saída"

#: ../xfce4-session-logout/main.c:78
msgid "Reboot without displaying the logout dialog"
msgstr "Reiniciar sem mostrar a caixa de diálogo de saída"

#: ../xfce4-session-logout/main.c:82
msgid "Suspend without displaying the logout dialog"
msgstr "Suspender sem mostrar a caixa de diálogo de saída"

#: ../xfce4-session-logout/main.c:86
msgid "Hibernate without displaying the logout dialog"
msgstr "Hibernar sem mostrar a caixa de diálogo de saída"

#: ../xfce4-session-logout/main.c:90
msgid "Hybrid Sleep without displaying the logout dialog"
msgstr "Suspensão híbrida sem mostrar o diálogo de saída"

#: ../xfce4-session-logout/main.c:94
msgid "Switch user without displaying the logout dialog"
msgstr "Trocar de utilizador sem mostrar a caixa de diálogo de saída"

#: ../xfce4-session-logout/main.c:98
msgid "Log out quickly; don't save the session"
msgstr "Sair rapidamente. Não guardar a sessão"

#: ../xfce4-session-logout/main.c:121
msgid "Unknown error"
msgstr "Erro desconhecido"

#: ../xfce4-session-logout/main.c:147
msgid "Written by Benedikt Meurer <benny@xfce.org>"
msgstr "Escrito por Benedikt Meurer <benny@xfce.org>"

#: ../xfce4-session-logout/main.c:148
msgid "and Brian Tarricone <kelnos@xfce.org>."
msgstr "e Brian Tarricone <kelnos@xfce.org>."

#: ../xfce4-session-logout/main.c:168 ../xfce4-session-logout/main.c:279
msgid "Received error while trying to log out"
msgstr "Recebido erro ao tentar sair da sessão"

#: ../xfce4-session-logout/main.c:244
#, c-format
msgid "Received error while trying to log out, error was %s"
msgstr "Recebido erro ao terminar sessão, erro foi %s"

#: ../xfce4-session-logout/xfce4-session-logout.desktop.in.h:2
msgid "Log out of the Xfce Desktop"
msgstr "Sair do ambiente de trabalho Xfce"

#: ../settings/xfce-session-settings.desktop.in.h:1
#: ../settings/xfce4-session-settings.ui.h:4
msgid "Session and Startup"
msgstr "Sessão e arranque"

#: ../settings/xfce-session-settings.desktop.in.h:2
msgid "Customize desktop startup"
msgstr "Personalizar arranque do ambiente de trabalho"

#: ../settings/xfce-session-settings.desktop.in.h:3
msgid ""
"session;settings;preferences;manager;startup;login;logout;shutdown;lock "
"screen;application;autostart;launch;services;daemon;agent;"
msgstr "sessão;definições;preferências;gestor;iniciar;entrar;sair;desligar;bloquear ecrã;aplicação;iniciar automático;lançar;serviços;daemon;agente;"

#: ../settings/xfce4-session-settings.ui.h:1
msgid ""
"These applications are a part of the currently-running session,\n"
"and can be saved now or when you log out.\n"
"Changes below will only take effect when the session is saved."
msgstr "Estas aplicações fazem parte da sessão atual e podem\nser guardadas agora ou quando sair da sessão.\nAs alterações abaixo só produzirão efeito quando a sessão for guardada."

#: ../settings/xfce4-session-settings.ui.h:5
msgid "_Help"
msgstr "_Ajuda"

#: ../settings/xfce4-session-settings.ui.h:7
msgid "_Display chooser on login"
msgstr "_Mostrar seletor ao iniciar sessão"

#: ../settings/xfce4-session-settings.ui.h:8
msgid "Display the session chooser every time Xfce starts"
msgstr "Exibir o seletor de sessões ao iniciar o Xfce"

#: ../settings/xfce4-session-settings.ui.h:9
msgid "<b>Session Chooser</b>"
msgstr "<b>Seletor de sessões</b>"

#: ../settings/xfce4-session-settings.ui.h:10
msgid "Automatically save session on logo_ut"
msgstr "Automaticamente gravar sessão ao sai_r"

#: ../settings/xfce4-session-settings.ui.h:11
msgid "Always save the session when logging out"
msgstr "Ao sair, guardar sempre a sessão"

#: ../settings/xfce4-session-settings.ui.h:12
msgid "Pro_mpt on logout"
msgstr "Confir_mar ao sair"

#: ../settings/xfce4-session-settings.ui.h:13
msgid "Prompt for confirmation when logging out"
msgstr "Pedir confirmação ao sair da sessão"

#: ../settings/xfce4-session-settings.ui.h:14
msgid "<b>Logout Settings</b>"
msgstr "<b>Definições de saída</b>"

#: ../settings/xfce4-session-settings.ui.h:15
msgid "Lock screen be_fore sleep"
msgstr "_Bloquear ecrã antes de suspender"

#: ../settings/xfce4-session-settings.ui.h:16
msgid "Run xflock4 before suspending or hibernating the system"
msgstr "Executar xflock4 antes de suspender ou hibernar o sistema"

#: ../settings/xfce4-session-settings.ui.h:17
msgid "<b>Shutdown</b>"
msgstr "<b>Desligar</b>"

#: ../settings/xfce4-session-settings.ui.h:18
msgid "_General"
msgstr "_Geral"

#: ../settings/xfce4-session-settings.ui.h:19
msgid "Save Sess_ion"
msgstr "Gravar sessã_o"

#: ../settings/xfce4-session-settings.ui.h:20
msgid "Currently active session: <b>Default</b>"
msgstr "Sessão ativa atualmente: <b>Predefinição</b>"

#: ../settings/xfce4-session-settings.ui.h:22
msgid "Current Sessio_n"
msgstr "Sessã_o atual"

#: ../settings/xfce4-session-settings.ui.h:23
msgid "_Remove"
msgstr "_Remover"

#: ../settings/xfce4-session-settings.ui.h:24
msgid "Delete the selected session"
msgstr "Eliminar a sessão selecionada"

#: ../settings/xfce4-session-settings.ui.h:25
msgid "Clear Save_d Sessions"
msgstr "Limpar sessões guar_dadas"

#: ../settings/xfce4-session-settings.ui.h:26
msgid "Saved _Sessions"
msgstr "Sessões _gravadas"

#: ../settings/xfce4-session-settings.ui.h:27
msgid "Launch GN_OME services on startup"
msgstr "Lançar serviços GN_OME no arranque"

#: ../settings/xfce4-session-settings.ui.h:28
msgid "Start GNOME services, such as gnome-keyring"
msgstr "Iniciar serviços GNOME, como gnome-keyring"

#: ../settings/xfce4-session-settings.ui.h:29
msgid "Launch _KDE services on startup"
msgstr "Lançar serviços _KDE no arranque"

#: ../settings/xfce4-session-settings.ui.h:30
msgid "Start KDE services, such as kdeinit"
msgstr "Iniciar serviços KDE, como \"kdeinit\""

#: ../settings/xfce4-session-settings.ui.h:31
msgid "<b>Compatibility</b>"
msgstr "<b>Compatibilidade</b>"

#: ../settings/xfce4-session-settings.ui.h:32
msgid "Manage _remote applications"
msgstr "Gerir aplicações _remotas"

#: ../settings/xfce4-session-settings.ui.h:33
msgid ""
"Manage remote applications over the network (this may be a security risk)"
msgstr "Gerir aplicações remotas através da rede (pode ser um risco de segurança)"

#: ../settings/xfce4-session-settings.ui.h:34
msgid "<b>Security</b>"
msgstr "<b>Segurança</b>"

#: ../settings/xfce4-session-settings.ui.h:35
msgid "Ad_vanced"
msgstr "A_vançado"

#: ../settings/xfce4-session-settings.ui.h:36
msgid "Saving Session"
msgstr "Gravando sessão"

#: ../settings/xfce4-session-settings.ui.h:37
msgid ""
"Your session is being saved.  If you do not wish to wait, you may close this"
" window."
msgstr "A sua sessão está a ser gravada. Se não quiser esperar, pode fechar esta janela."
